//
//  ViewController.swift
//  Idade de Cachorro
//
//  Created by Djeison Aquino on 16/08/17.
//  Copyright © 2017 Djeison Aquino. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }
    
    @IBOutlet weak var campoIdade: UITextField!
    @IBOutlet weak var resultado: UILabel!
    
    @IBAction func descobrirIdade(_ sender: Any) {
        
        var idadeCachorro = Int(campoIdade.text!)!
        
        idadeCachorro = idadeCachorro * 7
        
        resultado.text = "A idade do cachorro é " + String(idadeCachorro)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

